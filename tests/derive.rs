use compiletest_rs as compiletest;

fn run_mode(mode: &'static str) {
    let mut config = compiletest::Config::default();
    config.mode = mode.parse().expect("invalid compiletest mode");
    config.src_base = format!("tests/{}", mode).into();
    config.link_deps();
    config.clean_rmeta();

    compiletest::run_tests(&config);
}

#[test]
fn compile_tests() {
    run_mode("compile-fail");
    run_mode("run-pass");
}
