extern crate structview;

use structview::{u16_be, View};

#[derive(Clone, Copy, View)]
#[repr(C)]
struct Test([u8; 4], u16_be);

fn main() {}
