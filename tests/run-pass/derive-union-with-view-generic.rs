extern crate structview;

use structview::View;

#[derive(Clone, Copy, View)]
#[repr(C)]
union Test<T: View> {
    foo: T,
}

fn main() {}
