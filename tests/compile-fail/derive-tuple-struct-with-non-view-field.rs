extern crate structview;

use structview::View;

#[derive(Clone, Copy, View)]
#[repr(C)]
struct Test([u8; 4], u16); //~ ERROR the trait bound `u16: structview::View` is not satisfied


fn main() {}
