extern crate structview;

use structview::View;

#[derive(Clone, Copy, View)]
#[repr(C)]
struct Test<T: Copy> {
    foo: T,  //~ ERROR the trait bound `T: structview::View` is not satisfied
}

fn main() {}
