extern crate structview;

use structview::View;

#[derive(Clone, Copy, View)] //~ ERROR enums cannot derive `structview::View`
enum Test {
    Foo,
    Bar(u8),
}

fn main() {}
